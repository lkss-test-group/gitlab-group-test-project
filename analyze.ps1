sonar-scanner `
    -D"sonar.host.url=https://sonarcloud.io/" `
    -D"sonar.organization=lkss-test-group" `
    -D"sonar.projectKey=lkss-test-group_gitlab-group-test-project" `
    -D"sonar.login=$ENV:SONARCLOUD_TOKEN" `
    -D"sonar.python.version=3" `
    -D"sonar.sources=src"